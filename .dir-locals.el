((nil . (
         (projectile-project-compilation-dir . "/")
         (projectile-project-compilation-cmd . #'haskell-process-cabal-build)
         (projectile-project-test-cmd . (lambda ()
                                          (haskell-process-do-cabal
                                           "test --show-details=always --test-options=\"-a 25000 --maximum-unsuitable-generated-tests=100000 --maximum-test-depth=100000\"")))
         )
      )
 )
