module Generics
    (
     module GHC.Generics,
     module Data.Typeable
    ) where

import GHC.Generics (Generic)
import Data.Typeable (Typeable)
