module Text (
     module Data.Text,
     module Data.Text.Lens,
     module Data.Text.ICU.Normalized.NFC
     ) where

import Data.Text.ICU.Normalized.NFC
import Data.Text (Text, null, empty)
import Data.Text.Lens
