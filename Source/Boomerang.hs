{-# LANGUAGE TemplateHaskell #-}
{-# Language TypeOperators, PolyKinds, MultiWayIf #-}

module Boomerang (
    module Text.Boomerang,
    module Text.Boomerang.TH,
    module Text.Boomerang.Texts,
    module Control.Category,
    TextsBoomerang,
    (∘)
    ) where

import Text (Text)
import Text.Boomerang
import Text.Boomerang.TH
import Text.Boomerang.Texts
import Control.Category ((.), id, Category)

type TextsBoomerang a b = Boomerang TextsError [Text] a b

(∘) ∷ ∀ (b ∷ k) (c ∷ k) (a ∷ k) (cat ∷ k → k → *).
    Category cat ⇒ cat b c → cat a b → cat a c
(∘) = (.)
infix 9 ∘
