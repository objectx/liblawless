{-# LANGUAGE TemplateHaskell #-}
{-|
Module:             Text.Machines
Description:        Machines for transducing Text streams.
Copyright:          © 2016 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <>
Stability:          experimental
Portability:        POSIX
-}

module Text.Machine (
    readLines,
    writeLines
    ) where

import Lawless
import IO
import Text
import Text.IO
import Machine

-- | Read lines from a 'Handle' until $EOF$ is reached.
readLines ∷ MonadIO m ⇒ Handle → SourceT m Text
readLines h =
    let
        p = ifM (liftIO $ hIsEOF h) stop ((liftIO $ hGetLine h) >>= yield >> p)
    in
        construct p

-- | Write lines to a 'Handle' until there are no more. Forwards them
-- on.
writeLines ∷ MonadIO m ⇒ Handle → ProcessT m Text Text
writeLines h =
    let
        w = await >>= liftIO ∘ hPutStrLn h >> w
    in
        construct w
