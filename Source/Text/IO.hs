{-|
Module:             Text.IO
Description:        IO for Text handling.
Copyright:          © 2016 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <>
Stability:          experimental
Portability:        POSIX
-}

module Text.IO (
    readFile,
    writeFile,
    appendFile,
    hGetLine,
    hPutStr,
    hPutStrLn
    ) where

import Lawless
import Prelude.Unicode ((∘))
import qualified Data.Text.IO as TIO
import Data.Text (Text)
import System.Path
import Control.Monad.IO.Class
import System.Path.IO (Handle)

readFile ∷ (MonadIO m) ⇒ AbsRelFile → m Text
readFile = liftIO ∘ TIO.readFile ∘ toString

writeFile ∷ (MonadIO m) ⇒ AbsRelFile → Text → m ()
writeFile f t = liftIO $ TIO.writeFile (toString f) $ t

appendFile ∷ (MonadIO m) ⇒ AbsRelFile → Text → m ()
appendFile f t = liftIO $ TIO.appendFile (toString f) t

hPutStr ∷ (MonadIO m) ⇒ Handle → Text → m ()
hPutStr h = liftIO ∘ TIO.hPutStr h

hPutStrLn ∷ (MonadIO m) ⇒ Handle → Text → m ()
hPutStrLn h = liftIO ∘ TIO.hPutStrLn h

hGetLine ∷ (MonadIO m) ⇒ Handle → m Text
hGetLine = liftIO ∘ TIO.hGetLine
