{-# LANGUAGE DeriveFoldable #-}
module Textual
    (
     module Data.Textual,
     module Text.Printer
    ) where


import Data.Textual
import Text.Printer hiding ((<>))
