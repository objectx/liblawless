{-|
Module:             ByteString
Description:        Rexports ByteString so we don't have to carry it around.
Copyright:          © 2016 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module ByteString (
    module Data.ByteString
    ) where

import Data.ByteString
