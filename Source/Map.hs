module Map (
    module Data.Map.Unicode,
    Map,
    singleton
    ) where

import Data.Map.Lens()
import Data.Map.Unicode
import Data.Map.Strict (Map, singleton)
