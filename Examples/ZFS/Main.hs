{-|
Module:             Main
Description:        Demonstration of Boomerang and Yaml for parsing.
Copyright:          © 2016 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <>
Stability:          experimental
Portability:        POSIX
-}

module Main where

import Lawless
import Types
import IO
import Textual

main = putStrLn ("Hello" ∷ [Char])
