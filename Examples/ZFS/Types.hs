{-|
Module:             Types
Description:        ZFS and ZPool types
Copyright:          © 2016 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <>
Stability:          experimental
Portability:        POSIX
-}

module Types (
    module Types.ZPools,
    module Types.ZPool,
    module Types.ZName
    ) where

import Types.ZPools
import Types.ZPool
import Types.ZName
