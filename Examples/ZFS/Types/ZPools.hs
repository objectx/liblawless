{-|
Module:             Types.ZPools
Description:        Representation of a collection of ZPools
Copyright:          © 2016 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <>
Stability:          experimental
Portability:        POSIX
-}

{-# LANGUAGE TemplateHaskell #-}

module Types.ZPools where

import Lawless
import Aeson
import Set
import Types.ZPool

newtype ZPools = ZPools (Set ZPool)
    deriving (Eq, Show, Generic, Monoid)
makePrisms ''ZPools

instance FromJSON ZPools where
    parseJSON = lawlessParseJSON

instance ToJSON ZPools where
    toEncoding = lawlessToJSONEncoding
