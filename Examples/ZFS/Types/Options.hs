{-# Language TemplateHaskell, NoImplicitPrelude, GeneralizedNewtypeDeriving, TypeOperators #-}

-- | This module shows how to use 'Boomerang' for generating two-way
-- parsers.

module Options where

import Lawless hiding ((<>), (∘), (.))
import Text (Text)
import Boomerang

data  OnOff =
    On |
    Off
    deriving (Eq, Show, Ord)
makeBoomerangs ''OnOff

onOff ∷ TextsBoomerang () (OnOff :- ())
onOff =
    (
        rOn ∘ "on"
        <>
        rOff ∘ "off"
    )

data OnOffNoAuto =
    OOAOO OnOff |
    NoAuto
    deriving (Eq, Show, Ord)
makeBoomerangs ''OnOffNoAuto

onOffNoAuto =
    (
        rOOAOO ∘ onOff
        <>
        rNoAuto ∘ "noauto"
    )

data ACLType =
    NoACL |
    PosixACL
    deriving (Eq, Ord, Show)
makeBoomerangs ''ACLType

aclType ∷ TextsBoomerang () (ACLType :- ())
aclType =
    (
        rNoACL ∘ "noacl"
        <>
        rPosixACL ∘ "posixacl"
    )

data Checksum =
    ChkOnOff OnOff |
    Fletcher2 |
    Fletcher4 |
    SHA256
    deriving (Eq, Ord, Show)
makeBoomerangs ''Checksum

checksum ∷ TextsBoomerang () (Checksum :- ())
checksum =
    (
        rChkOnOff ∘ onOff
        <>
        rFletcher2 ∘ "fletcher2"
        <>
        rFletcher4 ∘ "fletechr4"
        <>
        rSHA256 ∘ "sha256"
    )

data GZn =
    G1 | G2 | G3 | G4 | G5 | G6 | G7 | G8 | G9
    deriving (Show, Eq, Ord, Enum)
makeBoomerangs ''GZn

gzN ∷ TextsBoomerang () (GZn :- ())
gzN =
    (
        rG1 ∘ "1" <>
        rG2 ∘ "2" <>
        rG3 ∘ "3" <>
        rG4 ∘ "4" <>
        rG5 ∘ "5" <>
        rG6 ∘ "6" <>
        rG7 ∘ "7" <>
        rG8 ∘ "8" <>
        rG9 ∘ "9"
      )

data Compression =
    CompressionOnOff OnOff |
    LZJB |
    LZ4 |
    GZIP |
    GZIPn GZn |
    ZLE
    deriving (Eq, Ord, Show)
makeBoomerangs ''Compression

compression ∷ TextsBoomerang () (Compression :- ())
compression =
    (
        rCompressionOnOff ∘ onOff <>
        rLZJB ∘ "lzjb" <>
        rLZ4 ∘ "lz4" <>
        rGZIP ∘ "gzip" <>
        rGZIPn ∘ ("gzip-" ∘ gzN) <>
        rZLE ∘ "zle"
    )

data Copies =
    C1 | C2 | C3
    deriving (Eq, Ord, Show)
makeBoomerangs ''Copies

copies ∷ TextsBoomerang () (Copies :- ())
copies =
    (
        rC1 ∘ "1" <>
        rC2 ∘ "2" <>
        rC3 ∘ "3"
    )

data Dedup =
    DOnOff OnOff |
    DVerify |
    DSHA256
    deriving (Eq, Ord, Show)
makeBoomerangs ''Dedup

dedup ∷ TextsBoomerang () (Dedup :- ())
dedup =
    (
        rDOnOff ∘ onOff <>
        rDVerify ∘ "verify" <>
        rDSHA256 ∘ "sha256" <>
        rDVerify ∘ "sha256,verify"
    )

data Devices = Devices OnOff deriving (Show, Eq, Ord)
makeBoomerangs ''Devices

devices ∷ TextsBoomerang () (Devices :- ())
devices =
    (
        rDevices ∘ onOff
    )

data Exec = Exec OnOff deriving (Show, Eq, Ord)
makeBoomerangs ''Exec

exec ∷ TextsBoomerang () (Exec :- ())
exec =
    (
        rExec ∘ onOff
    )

data Mountpoint =
    RelMP RelDir |
    AbsMP AbsDir |
    NoMP
    deriving (Show, Eq, Ord)
makeBoomerangs ''Mountpoint

mountpoint ∷ TextsBoomerang () (Mountpoint :- ())
mountpoint =
    (
        ((rRelMP ∘ parseRelDir) <$> anyText) <>
        ((rAbsMP ∘ parseAbsDir) <$> anyText) <>
        rNoMP ∘ "none"
    )

data ZFSProperty =
    ZACLType ACLType |
    ZATime OnOff |
    ZCanMount OnOffNoAuto |
    ZChecksum Checksum |
    ZCompression Compression |
    ZCopies Copies |
    ZDedup Dedup |
    ZDevices Devices |
    ZExec Exec
