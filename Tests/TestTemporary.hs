{-# LANGUAGE TemplateHaskell, OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-orphans #-}

{-|
Module:             TestTemporary
Description:        Test that temporary files are unbuffered and binary.
Copyright:          © 2016 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <>
Stability:          experimental
Portability:        POSIX
-}

module TestTemporary where

import Lawless
import Path
import Text
import Text.IO
import IO
import Control.Concurrent.STM

import Arbitrary()
import Test.Framework
import Test.Framework.TH
import Test.Framework.Providers.QuickCheck2 (testProperty)
import Test.QuickCheck
import Test.QuickCheck.Monadic
default (Text)

newtype Line = Line Text deriving (Eq, Ord, Show)
instance Arbitrary Line where
    arbitrary = Line <$> suchThat arbitrary (not ∘ anyOf each (≡ '\n'))

prop_CheckBuffering ∷ Line → Property
prop_CheckBuffering (Line line) = monadicIO $ do
    m ← run $ do
        v ← atomically $ newEmptyTMVar
        runManaged $ do
            tf ← tempFile (absDir ("/tmp" ∷ Text)) (relFile ("testTemp" ∷ Text))
            let h = tf ^. tfHandle
            liftIO $ hPutStrLn h line
            liftIO $ hSeek h AbsoluteSeek 0
            liftIO $ hGetLine h >>= atomically ∘ putTMVar v
        liftIO $ atomically $ takeTMVar v
    assert (line ≡ m)

properties ∷ Test
properties = $(testGroupGenerator)
