# liblawless: An Effectful Foundation

## Overview
[liblawless][liblawless] is a replacement for the
standard [Prelude][prelude]. It targets [GHC 8.0][ghc80] and
newer. It's core is building a fine-grained but readily accessible
Effect model to move more type checking of code that changes its
environment out of plain IO.

[prelude]: http://hackage.haskell.org/package/base
[liblawless]: https://www.lambdanow.us/wiki/LearningProjects/liblawless
[ghc80]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/index.html

## Pure vs Effectful Functions
Pure functions don't affect the anything outside of the function. A
Pure function will run a calculation on the given values, and return a
result. If you pass in the same parameters, you'll always get the same
result no matter how many times you call the function.

Effectful functions are functions that do affect the program outside
the function, and often the environment outside the computer running
them. Even with the same parameters, Effectful Functions can return
different results. In many cases they can even return signals that
completing their task wasn't possible. In many languages these are
called "Side Effects", and aren't modeled in the type system at
all. Haskell comes with a simple Effect model in its type system,
the [io][IO monad].

[io]: http://hackage.haskell.org/package/base-4.9.0.0/docs/System-IO.html#t:IO

## Kinds of Effects
The [io][IO monad] models Effects a function has on the world outside
the program. There are other, more limited Effects as well, and there
are libraries for managing these as well. The two most common are:

- [transformers][Transformers]
- [mtl][The MTL]

[transformers]: http://hackage.haskell.org/package/transformers Transformers
[mtl]: http://hackage.haskell.org/package/mtl

These model Effects that only apply to the current program, while the
IO monad models ''all other effects'' on the world. That's a really
broad brush. Most bugs in Haskell code are in the IO monad.

This project is implementing several extra types of Effects that
affect the world. Instead of treating them all the same, though, it
breaks them up into much smaller kinds of Effects. For example, for
accessing files, it's possible to:

- read bytes from a file
- write bytes to a file
- read lines of text from a file
- write lines of text from a file
- read arbitrary data from a file, operate on these data items, and
  write them to a network stream
- many other simple and complex operations

We are building them on top of the [machines][Machines] library. This
library offers a composable model for connecting strongly-typed
streams together. It also implements provisions for arbitrary effects,
and connecting Effectful functions with Pure functions into streams of
computations. When combined with the [async][Async] library, these
streams, and even individual nodes in these streams, can be run
concurrently and safely, making full use of multicore systems.

[machines]: http://hackage.haskell.org/package/machines
[async]: http://hackage.haskell.org/package/async
